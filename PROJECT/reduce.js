function reduce(elements, cb,startingValue){
    if (startingValue===undefined){
        startingValue=elements[0];
    }
    
    for (let i=0;i<elements.length;i++){
        let element=elements[i];
        var sum=cb(startingValue,element);
        startingValue=sum;
    }
    return sum;
}


function cb(startingValue,element){
    startingValue=startingValue+element;
    return startingValue;

}


module.exports={reduce,cb};