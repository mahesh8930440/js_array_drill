function flatten(elements, cb){
    var flattenElements=[];
    for (let i=0;i<elements.length;i++){
        let element=elements[i];
        var flattenElement=cb(element);
        flattenElements.push(flattenElement);
    }
    return flattenElements;
}

function cb(element){
    
    if (Array.isArray(element)){
        for (let i=0;i<element.length;i++){
            cb(element[i]);
        }
    }
    else{
        return element;
    }
}

module.exports={flatten,cb};