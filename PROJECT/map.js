function map(elements, cb){
    const squaresElements=[];
    for (let i=0;i<elements.length;i++){
        let element=elements[i];
        var squaresElement=cb(element);
        squaresElements.push(squaresElement)
    }
    return squaresElements;
}

function cb(element){
    let newElement=element**2;
    return newElement;
}

module.exports={map,cb};