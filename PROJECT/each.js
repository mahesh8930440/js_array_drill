function each(elements, cb){
    for (let i=0;i<elements.length;i++){
        let element=elements[i];
        let index=i;
        
        cb(element,index);
    }
}
function cb(element,index){
    console.log("The Element at index "+index+" is " + element);
}
module.exports={each,cb};