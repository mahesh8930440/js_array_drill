function filter(elements, cb){
    var filterElements=[];
    
    for (let i=0;i<elements.length;i++){
        let element=elements[i];
        let filterElement=cb(element);
        
        if (filterElement!==undefined){
            filterElements.push(filterElement);
        } 
    }
    return filterElements;
}

function cb(element){
    
    if (element%2===0){
        return element;
    }
    
    
}
module.exports={filter,cb};
